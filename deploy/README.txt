How to build checkit-tiff variants for Redhat 7 system used by Rosetta
======================================================================

:lang: en
:encoding: utf-8
:author: Jörg Sachse

== Prerequisites

You need the following binaries installed on your system:
* `bash`
* `docker`
* `grep`
* `id`
While most of these will come with your Linux distribution, you might need to install Docker from their official 3rd party repository. Please refer to the https://docs.docker.com/engine/install/[relevant documentation at the Docker website].

== How it works

This script will create a Docker image that contains all the necessary build tools. It will then run the container and download all necessary software repositories and dependencies to build `checkit_tiff` binaries and packages for various package managers and release versions (called "current" and "upcoming"). When the build process is complete, you can find all binaries and packages in the `artifacts` subdirectory. The container will be stopped and will consume no more CPU resources.

If you have used the script before, there's a chance that the Docker image and container are still on your computer. You can choose to keep them and just restart the container to build new binaries (this is the default), or you can replace them with updated versions.

== How to configure

You can set various options using the `create_packages.cfg` file. If you set these, they will overwrite default values that are hardcoded, but will themselves be overwritten by CLI parameters. The config file contains detailed explanations for each of the available parametes.

== How to run

If you need help, you can run the script with:
[source, bash]
----
./create_packages.sh --help
----

You can choose to:
* keep or rebuild the image and container
* create statically or dynamically linked binaries
* enable or disable various ICC profile checks
* set custom versions for "current" and "upcoming"

[TIP]
====
You can use `lazydocker` to get a deeper insight into what your docker daemon is doing. Get it at https://github.com/jesseduffield/lazydocker[lazydocker's GitHub repository].
====

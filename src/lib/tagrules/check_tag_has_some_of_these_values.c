/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#include "check.h"
#include "check_helper.h"
#include "ctstring.h"
/*
#define DEBUG
*/

ret_t check_tag_has_some_of_these_values(ctiff_t * ctif, tag_t tag, unsigned int count, const unsigned int * values) {
  GET_EMPTY_RET(ret);
  TIFP_CHECK( ctif, ret);
  EXIST_TAG(ctif, tag, ret);
  const unsigned int * p = values;
  TIFFDataType datatype =  TIFFGetRawTagType( ctif, ctif->ifd0, tag );
  ret.returncode=should_not_occur;
  switch (datatype) {
    case TIFF_LONG: {
                      p = values;
                      ret_t tmp_res;
                      for (unsigned int i=0; i< count; i++) {
#ifdef DEBUG
                        printf("### value = %u", *p);
#endif
                        tmp_res = check_tag_has_u32value(ctif, ctif->ifd0, tag, *p);
                        if (tmp_res.returncode == 0) return tmp_res;
                        p++;
                      }
                      uint32 * valp = NULL;
                      uint32 vcount=0;
                      ret = TIFFGetFieldLONG(ctif, ctif->ifd0, tag, &valp, &vcount);
                      if (vcount >0) {
                        ret = set_value_found_ret_u32_value(&ret, *valp);
                        ret.returncode = tagerror_value_differs;
                      }
                      return ret;
                      /* break; */
                    }
    case TIFF_SHORT: {
                       p = values;
                       ret_t tmp_res;
                       for (unsigned int i=0; i< count; i++) {
#ifdef DEBUG
                         printf("### value = %u", *p);
#endif
                         tmp_res = check_tag_has_u16value(ctif, ctif->ifd0, tag, *p);
                         if (tmp_res.returncode == 0) return tmp_res;
                         p++;
                       }
                       uint16 * valp = NULL;
                       uint32 vcount=0;
                       ret = TIFFGetFieldSHORT(ctif, ctif->ifd0, tag, &valp, &vcount);
                       if (vcount >0) {
                         ret = set_value_found_ret_u16_value(&ret, *valp);
                         ret.returncode = tagerror_value_differs;
                       }
                       return ret;
                       /* break; */
                     }
    case TIFF_RATIONAL: {
                          p = values;
                          ret_t tmp_res;
                          for (unsigned int i=0; i< count; i++) {
#ifdef DEBUG
                            printf("### value = %u", *p);
#endif
                            tmp_res = check_tag_has_fvalue(ctif, ctif->ifd0, tag, (float) *p);
                            if (tmp_res.returncode == 0) return tmp_res;
                            p++;
                          }
                          float * valp = NULL;
                          uint32 vcount=0;
                          ret = TIFFGetFieldRATIONAL(ctif, ctif->ifd0, tag, &valp, &vcount);
                          if (count >0) {
                            float val = * valp;
                            ret = set_value_found_ret_float_value( &ret, val);
                            ret.returncode = tagerror_value_differs;
                          }
                          return ret;
                          /* break; */
                        }
    default: /*  none */
                        {
                          ret.value_found = const_str(TIFFTypeName(datatype));
                          ret.returncode = tagerror_unexpected_type_found;
                        }
  }
  assert( ret.returncode != should_not_occur);
  return ret;
}

/* vim: set tabstop=2 softtabstop=2 shiftwidth=2 smarttab expandtab :*/

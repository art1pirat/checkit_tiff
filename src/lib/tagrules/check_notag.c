/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#include "check.h"
#include "check_helper.h"
#include "ctstring.h"
/* #define DEBUG */

/* checks if TIF does not have a specified tag,
 * needed only for checks to ensure whitelist */
ret_t check_notag(ctiff_t * ctif, tag_t tag) {
  GET_EMPTY_RET(ret);
  TIFP_CHECK( ctif, ret);
  if (no_index_found == TIFFGetRawTagListIndex(ctif->ifd0, tag)) {
      ret.returncode = is_valid;
  } else {
    ret = set_value_found_ret_formatted_with_returncode(&ret,  tagerror_not_white_listed, "tag %i exists", tag);
  }
  return ret;
}
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2 smarttab expandtab :*/

/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#include "check.h"
#include "check_helper.h"
/* #define DEBUG */


/* checks if TIF has a specified tag */
returncode_t check_tag_quiet(ctiff_t * ctif, tag_t tag) {
  if (no_index_found != TIFFGetRawTagListIndex(ctif->ifd0, tag)) {
      return is_valid;
  } return tag_does_not_exist;
}
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2 smarttab expandtab :*/

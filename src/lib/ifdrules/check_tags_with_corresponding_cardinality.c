/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#include "check.h"
#include "check_helper.h"
#include "ctstring.h"

/* check if cardinality of tags are equal */
ret_t check_cardinality_of_some_tags_are_equal(ctiff_t * ctif) {
  /* we need to check following tag groups:
   * StripOffset and StripBytesCounts
   */
  GET_EMPTY_RET(ret);
  TIFP_CHECK( ctif, ret);
  /* check if both values exist */
  returncode_t rc_stripoffsets=check_tag_quiet(ctif, TIFFTAG_STRIPOFFSETS);
  returncode_t rc_stripbytecounts =check_tag_quiet(ctif, TIFFTAG_STRIPBYTECOUNTS);
  if ((rc_stripoffsets == is_valid) && (rc_stripbytecounts == is_valid)) {
      /* find stripoffset */
      ifd_entry_t stripoffsets_entry = TIFFGetRawIFDEntry(ctif, ctif->ifd0, TIFFTAG_STRIPOFFSETS);
      ifd_entry_t stripbytescounts_entry = TIFFGetRawIFDEntry(ctif, ctif->ifd0, TIFFTAG_STRIPBYTECOUNTS);
      if (stripoffsets_entry.count != stripbytescounts_entry.count) {
          ret = set_value_found_ret_formatted_with_returncode(&ret, ifderror_different_cardinality, "tag %u count=%u, tag %u count=%u", TIFFTAG_STRIPOFFSETS, stripoffsets_entry.count, TIFFTAG_STRIPBYTECOUNTS, stripbytescounts_entry.count);
          return ret;
      }
  }
  ret.returncode=is_valid;
  return ret;
}
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2 smarttab expandtab :*/

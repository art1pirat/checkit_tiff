/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */


#include <assert.h>
#include "ctstring.h"
#include "check.h"
#include "check_helper.h"

/** adds a message to render pipeline
 * @pointer to render pipeline
 * @src message
 * @src_type message rm_type
 */
returncode_t add_to_render_pipeline_via_strncpy (retmsg_t ** pointer, string_t src, rm_type_t src_type) {
   assert(pointer != NULL);
   retmsg_t * actual_render = NULL;
   actual_render = *pointer;
   assert(actual_render != NULL);
   assert(actual_render->next==NULL);
   retmsg_t * next = malloc ( sizeof(retmsg_t));
   if (NULL == next) {
     exit( could_not_allocate_memory);
     // return could_not_allocate_memory;
   }
   actual_render->next = next;
   actual_render->next->rm_msg = src;
   actual_render->next->rm_type = src_type;
   // fprintf(stderr, "rendertype=%i rendermsg='%s'\n",actual_render->next->rm_type, actual_render->next->rm_msg );
   actual_render->next->next=NULL;
   actual_render = actual_render->next;
   assert(actual_render != NULL);
   *pointer = actual_render;
   return is_valid;
}

void clean_render_pipeline(retmsg_t **pointer ) {
  assert(pointer != NULL);
  retmsg_t * actual_render = *pointer;
  while (NULL != actual_render) {
    retmsg_t * next = actual_render->next;
    clean_str(& actual_render->rm_msg);
    free( actual_render );
    actual_render = next;
  }
}

/*
returncode_t tifp_check( ctiff_t * ctif) {
  if (NULL == ctif) { return code_error_ctif_empty; };
  if (0 > ctif->fd) { return code_error_filedescriptor_empty; };
  if (NULL == ctif->streamp) { return code_error_streampointer_empty; };
  return should_not_occur;
}
*/

/* vim: set tabstop=2 softtabstop=2 shiftwidth=2 smarttab expandtab :*/

/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#include <stdbool.h>
#include "check.h"
#include "check_renderer.h"
#include "ctstring.h"

static render_type render_engine=render_default;
static bool is_quiet = false;

void set_renderer_to_quiet(void) {
  is_quiet=true;
}

void set_renderer_to_ansi(void) {
  if (isatty (STDOUT_FILENO)) {
    render_engine=render_ansi;
  }
}

void set_renderer_to_csv(void) {
    render_engine=render_csv;
}

bool check_if_quiet(const retmsg_t *startp, rc_tristate_t rc_is_valid) {
    return is_quiet
           && (rc_valid == rc_is_valid)
           && (startp->rm_type != rm_file)
           && (startp->rm_type != rm_hard_error)
           && (startp->rm_type != rm_error)
           && (startp->rm_type != rm_logicalor_error)
           && (startp->rm_type != rm_count_invalid)
           && (startp->rm_type != rm_count_valid)
           && (startp->rm_type != rm_summary_invalid)
           && (startp->rm_type != rm_summary_valid);
}

stringbuf_t renderer ( const retmsg_t * ret ) {
  // call debug renderer
  switch (render_engine) {
    case render_ansi: return renderer_ansi( ret);
    case render_csv: return renderer_csv( ret);
    default: return renderer_default( ret);
  }
  return empty_stringbuf();
}
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2 smarttab expandtab :*/

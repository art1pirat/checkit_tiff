/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#ifndef TIFF_RISKANALYZER
#define TIFF_RISKANALYZER

typedef enum {
    mt_unused, /* memory areas, which are not referenced within TIFF file */
    mt_constant, /* constant values, which are fix part of TIFF file */
    mt_ifd, /* memory areas, which are parts of the IFD (but no value!) */
    mt_offset_to_ifd0,
    mt_offset_to_ifd, /* offset to nex ifd */
    mt_ifd_embedded_standardized_value, /* memory areas, with standardized values embedded in ifd */
    mt_ifd_embedded_registered_value, /* memory areas, with registered values embedded in ifd */
    mt_ifd_embedded_private_value, /* memory areas, with private values embedded in ifd */
    mt_ifd_offset_to_standardized_value, /* memory areas, which points to standardized values */
    mt_ifd_offset_to_registered_value, /* memory areas, which points to registered values */
    mt_ifd_offset_to_private_value, /* memory areas, which points to private values */
    mt_ifd_offset_to_stripoffsets, /* offsets which points to stripoffsets */
    mt_stripoffset_value, /* offsets which points to stripoffset values, hint: if compression is used stripbytecounts holds only decompressed values! */
    mt_standardized_value, /* memory areas, which contains standardized values */
    mt_registered_value, /* memory areas, which contains registered values */
    mt_private_value, /* memory areas, which contains private values */
    mt_END_marker
} memtype_t;

static const char * memtype_string[] = {
        "unused/unknown", /* memory areas, which are not referenced within TIFF file */
        "constant", /* constant values, which are fix part of TIFF file */
        "ifd", /* memory areas, which are parts of the IFD (but no value!) */
        "offset_to_ifd0", /* offset to nex ifd */
        "offset_to_ifd", /* offset to nex ifd */
        "ifd_embedded_standardized_value", /* memory areas, with standardized values embedded in ifd */
        "ifd_embedded_registered_value", /* memory areas, with registered values embedded in ifd */
        "ifd_embedded_private_value", /* memory areas, with private values embedded in ifd */
        "ifd_offset_to_standardized_value", /* memory areas, which points to standardized values */
        "ifd_offset_to_registered_value", /* memory areas, which points to registered values */
        "ifd_offset_to_private_value", /* memory areas, which points to private values */
        "ifd_offset_to_stripoffsets", /* offsets which points to stripoffsets */
        "stripoffset_value", /* stripoffset values, hint: if compression is used stripbytecounts holds only decompressed values! */
        "standardized_value", /* memory areas, which contains standardized values */
        "registered_value", /* memory areas, which contains registered values */
        "private_value", /* memory areas, which contains private values */
};

typedef struct mem_map_entry_s {
    uint32 offset; /* address within the tiff */
    uint32 count; /* count of bytes beginning with offset */
    memtype_t mem_type; /* type of memory */
} mem_map_entry_t;

typedef struct mem_map_s {
    mem_map_entry_t * base_p;
    int count;
    int max_entries;
    uint32 max_len; /* TIFF length */
} mem_map_t;

mem_map_t * scan_mem_map(ctiff_t * ctif) ;
void print_mem_map(const mem_map_t *memmap_p);
void print_mem_stats(const mem_map_t *memmap_p);

#endif
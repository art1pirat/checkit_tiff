/* 'checkit_tiff' is a conformance checker for baseline TIFFs
 *
 * author: Andreas Romeyke, 2015-2022
 * licensed under conditions of libtiff
 * (see http://libtiff.maptools.org/misc.html)
 *
 */

#ifndef CHECKIT_TIFF_PARSER_TYPES
#define CHECKIT_TIFF_PARSER_TYPES
typedef enum { mandatory, ifdepends, optdepends, optional } requirements_t;
typedef enum { range, logical_or, any, only, regex, ntupel, sbit, iccprofile, datetime, printable_ascii } values_t;
typedef enum { no_ref, any_ref, only_ref, range_ref, ntupel_ref, regex_ref } reference_t;

typedef enum {
    mode_baseline=1,
    mode_enable_type_checks=2,
    mode_enable_offset_checks=4,
    mode_enable_ifd_checks=8,
    mode_enable_deep_geotiff_checks=16,
} modes_t;

typedef enum {
    st_LOW_GUARD=0,
    st_unsigned_int,
    st_regex,
/*    st_val, */
    st_HIGH_GUARD=99
} internal_stacktype_t;

typedef struct stat_s {
    size_t stat_push_count;
    size_t stat_pushback_count;
    size_t stat_realloc_count;
} stat_t;


#endif
